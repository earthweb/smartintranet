<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
    <meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://google.com/" />
    <meta property="og:site_name" content="DOA" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <title>เข้าสู่ระบบ | DOA Smart Intranet </title>

    <?php include 'layouts/inc-head.php' ?>
</head>

<body id="kt_body" class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <div class="d-flex flex-column flex-lg-row-auto w-xl-600px positon-xl-relative" style="background: rgb(6,115,231);background: linear-gradient(90deg, rgba(6,115,231,1) 35%, rgba(18,155,247,1) 100%);">
                <div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-600px scroll-y">
                    <div class="d-flex flex-row-fluid flex-column text-center p-10 pt-lg-20">
                        <a href="#" class="py-9 mb-5">
                            <img alt="Logo" src="assets/img/logo-main.png" class="" />
                        </a>
                        <h1 class="fw-bolder fs-2qx pb-5 pb-md-10 text-white" >ยินดีต้อนรับสู่ <br> DOA Smart Intranet</h1>
                        <p class="fw-bold fs-2 text-white" >ติดตามข้อมูลข่าวสารภายใน
                        </p>
                    </div>
                    <div class="d-flex flex-row-auto bgi-no-repeat bgi-position-x-center bgi-size-contain bgi-position-y-bottom min-h-100px min-h-lg-350px" style="background-image: url(assets/media/illustrations/unitedpalms-1/13.png"></div>
                </div>
            </div>
            <div class="d-flex flex-column flex-lg-row-fluid py-10">
                <div class="d-flex flex-center flex-column flex-column-fluid">
                    <div class="w-lg-500px p-10 p-lg-15 mx-auto">
                        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="#">
                            <div class="text-center mb-10">
                                <h1 class="text-dark mb-3">เข้าสู่ระบบ <br> DOA Smart Intranet</h1>
                            </div>
                            <div class="fv-row mb-10">
                                <label class="form-label fs-6 fw-bolder text-dark">ชื่อผู้ใช้งาน</label>
                                <input class="form-control form-control-lg form-control-solid" type="text" name="username" autocomplete="off" />
                            </div>
                            <div class="fv-row mb-10">
                                <div class="d-flex flex-stack mb-2">
                                    <label class="form-label fw-bolder text-dark fs-6 mb-0">รหัสผ่าน</label>
                                    <a href="reset-password.php" class="link-primary fs-6 fw-bolder">ลืมรหัสผ่าน</a>
                                </div>
                                <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" />
                            </div>
                            <div class="text-center">
                                <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                                    <span class="indicator-label">เข้าสู่ระบบ</span>
                                    <span class="indicator-progress">โปรดรอ...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
                    <div class="d-flex flex-center fw-bold fs-6">
                        <a href="#" class="text-muted text-hover-primary px-2" target="_blank">About</a>
                        <a href="#" class="text-muted text-hover-primary px-2" target="_blank">Support</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'layouts/inc-script.php' ?>
    <script src="assets/js/custom/authentication/sign-in/general.js"></script>
</body>

</html>