<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
    <meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <title>FAQ | DOA Smart Intranet</title>

    <?php include 'layouts/inc-head.php' ?>

</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <div class="d-flex flex-column flex-root">
        <div class="page d-flex flex-row flex-column-fluid">
            <?php include 'layouts/inc-aside.php' ?>
            <!--begin::Wrapper-->
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <?php include 'layouts/inc-header.php' ?>

                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <div class="toolbar" id="kt_toolbar">
                        <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                            <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                                <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">คำถามที่พบบ่อย
                                    <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
                                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                                        <li class="breadcrumb-item text-muted">
                                            <a href="index.php" class="text-muted text-hover-primary"><i class="bi bi-house-door-fill"></i></a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <span class="bullet bg-gray-200 w-5px h-2px"></span>
                                        </li>
                                        <li class="breadcrumb-item text-dark">คำถามที่พบบ่อย</li>
                                    </ul>
                                </h1>
                            </div>
                        </div>
                    </div>

                    <div class="post d-flex flex-column-fluid" id="kt_post">

                        <div id="kt_content_container" class="container-xxl">


                            <div class="card">
                                <div class="card-body p-lg-15">
                                    <div class="card-rounded bg-light d-flex flex-stack flex-wrap p-5">
                                        <ul class="nav flex-wrap border-transparent fw-bolder">
                                            <li class="nav-item my-1">
                                                <a href="#" class="btn btn-color-gray-600 btn-active-white btn-active-color-primary fw-boldest fs-8 fs-lg-base nav-link px-3 px-lg-8 mx-1 text-uppercase active">
                                                    <svg xmlns="http://www.w3.org/2000/svg" id="Capa_1" enable-background="new 0 0 512 512" height="16" viewBox="0 0 512 512" width="16">
                                                        <path d="m397 28.086h-282c-63.411 0-115 51.589-115 115v110c0 58.33 43.648 106.655 100 114.026v101.801c0 6.067 3.654 11.537 9.26 13.858 1.855.769 3.805 1.142 5.737 1.142 3.903 0 7.74-1.524 10.609-4.394l111.434-111.433h159.96c63.411 0 115-51.589 115-115v-110c0-63.411-51.589-115-115-115zm85 225c0 46.869-38.131 85-85 85h-166.173c-3.979 0-7.794 1.58-10.606 4.393l-90.221 90.221v-79.614c0-8.284-6.716-15-15-15-46.869 0-85-38.131-85-85v-110c0-46.869 38.131-85 85-85h282c46.869 0 85 38.131 85 85z"></path>
                                                        <path d="m182.7 155.856c6 0 9.4-6.4 9.4-13.8 0-6.4-2.801-13.4-9.4-13.4h-69.398c-6.801 0-13.6 3.2-13.6 9.6v126.998c0 6.4 7.799 9.6 15.6 9.6 7.799 0 15.6-3.2 15.6-9.6v-51.6h30.199c6.6 0 9.4-6.4 9.4-11.8 0-6.4-3.4-12.2-9.4-12.2h-30.199v-33.799h51.798z"></path>
                                                        <path d="m265.903 137.856c-2.199-7-11.199-10.4-20.4-10.4-9.199 0-18.199 3.4-20.398 10.4l-37.199 121.998c-.201.8-.4 1.6-.4 2.2 0 7.4 11.4 12.8 20 12.8 5 0 9-1.6 10.199-5.8l6.801-25h42.199l6.799 25c1.201 4.2 5.201 5.8 10.199 5.8 8.602 0 20-5.4 20-12.8 0-.6-.199-1.4-.398-2.2zm-35 82.198 14.6-53.599 14.6 53.599z"></path>
                                                        <path d="m372.7 128.656c-27.6 0-48.998 12.6-48.998 47.199v52.999c0 30.4 16.6 43.799 39.398 46.6v11.6c0 6.4 4.801 9.6 9.6 9.6 4.801 0 9.6-3.2 9.6-9.6v-11.6c22.801-2.8 39.6-16.2 39.6-46.6v-52.999c-.001-34.599-21.6-47.199-49.2-47.199zm18 100.198c0 9.2-3.199 15-8.4 18v-8.8c0-6.6-4.799-9.4-9.6-9.4-4.799 0-9.6 2.8-9.6 9.4v8.8c-5.4-3-8.199-8.8-8.199-18v-52.999c0-13.6 6.6-19.999 17.799-19.999 11.201 0 18 6.4 18 19.999z"></path>
                                                    </svg>
                                                    &nbsp;คำถามที่พบบ่อย</a>
                                            </li>
                                            <li class="nav-item my-1">
                                                <a href="#" class="btn btn-color-gray-600 fw-boldest fs-8 fs-lg-base nav-link px-3 px-lg-8 mx-1 text-uppercase active">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="16" height="16" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                                        <g>
                                                            <path d="M257.938,336.072c0,17.355-14.068,31.424-31.423,31.424c-17.354,0-31.422-14.068-31.422-31.424   c0-17.354,14.068-31.423,31.422-31.423C243.87,304.65,257.938,318.719,257.938,336.072z M385.485,304.65   c-17.354,0-31.423,14.068-31.423,31.424c0,17.354,14.069,31.422,31.423,31.422c17.354,0,31.424-14.068,31.424-31.422   C416.908,318.719,402.84,304.65,385.485,304.65z M612,318.557v59.719c0,29.982-24.305,54.287-54.288,54.287h-39.394   C479.283,540.947,379.604,606.412,306,606.412s-173.283-65.465-212.318-173.85H54.288C24.305,432.562,0,408.258,0,378.275v-59.719   c0-20.631,11.511-38.573,28.46-47.758c0.569-84.785,25.28-151.002,73.553-196.779C149.895,28.613,218.526,5.588,306,5.588   c87.474,0,156.105,23.025,203.987,68.43c48.272,45.777,72.982,111.995,73.553,196.779C600.489,279.983,612,297.925,612,318.557z    M497.099,336.271c0-13.969-0.715-27.094-1.771-39.812c-24.093-22.043-67.832-38.769-123.033-44.984   c7.248,8.15,13.509,18.871,17.306,32.983c-33.812-26.637-100.181-20.297-150.382-79.905c-2.878-3.329-5.367-6.51-7.519-9.417   c-0.025-0.035-0.053-0.062-0.078-0.096l0.006,0.002c-8.931-12.078-11.976-19.262-12.146-11.31   c-1.473,68.513-50.034,121.925-103.958,129.46c-0.341,7.535-0.62,15.143-0.62,23.08c0,28.959,4.729,55.352,12.769,79.137   c30.29,36.537,80.312,46.854,124.586,49.59c8.219-13.076,26.66-22.205,48.136-22.205c29.117,0,52.72,16.754,52.72,37.424   c0,20.668-23.604,37.422-52.72,37.422c-22.397,0-41.483-9.93-49.122-23.912c-30.943-1.799-64.959-7.074-95.276-21.391   C198.631,535.18,264.725,568.41,306,568.41C370.859,568.41,497.099,486.475,497.099,336.271z M550.855,264.269   C547.4,116.318,462.951,38.162,306,38.162S64.601,116.318,61.145,264.269h20.887c7.637-49.867,23.778-90.878,48.285-122.412   C169.37,91.609,228.478,66.13,306,66.13c77.522,0,136.63,25.479,175.685,75.727c24.505,31.533,40.647,72.545,48.284,122.412   H550.855L550.855,264.269z"></path>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                        <g>
                                                        </g>
                                                    </svg>
                                                    &nbsp;
                                                    แจ้งปัญหาการใช้งาน</a>
                                            </li>
                                            <li class="nav-item my-1">
                                                <a href="#" class="btn btn-color-gray-600 fw-boldest fs-8 fs-lg-base nav-link px-3 px-lg-8 mx-1 text-uppercase active">
                                                    นโยบายเว็บไซต์
                                                </a>
                                            </li>
                                            <li class="nav-item my-1">
                                                <a href="#" class="btn btn-color-gray-600 fw-boldest fs-8 fs-lg-base nav-link px-3 px-lg-8 mx-1 text-uppercase active">นโยบายการคุ้มครองข้อมูลส่วนบุคคล</a>
                                            </li>
                                        </ul>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#kt_modal_new_faq" class="btn btn-primary fw-bolder fs-8 fs-lg-base"><i class="bi bi-plus-circle"></i> สร้างคำถามที่พบบ่อย</a>
                                    </div>

                                    <div class="d-flex flex-column flex-lg-row">
                                        <div class="flex-column flex-lg-row-auto w-100 w-xl-300px mb-10">
                                            <div class="card m-2 shadow-none " data-kt-sticky="true" data-kt-sticky-name="docs-sticky-summary" data-kt-sticky-offset="{default: false, xl: '50px'}" data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="160px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95" style="">
                                                <div class="card-body">
                                                    <div class="mb-15">
                                                        <h4 class="text-black mb-7">หมวดหมู่คำถามที่พบบ่อย</h4>
                                                        <!--begin::Menu-->
                                                        <div class="menu menu-rounded menu-column menu-title-gray-700 menu-state-title-primary menu-active-bg-light-primary fw-bold">
                                                            <div class="menu-item mb-1">
                                                                <a href="#faq-1" class="menu-link py-3 active">การเข้าสู่ระบบ</a>
                                                            </div>
                                                            <div class="menu-item mb-1">
                                                                <a href="#faq-1" class="menu-link py-3">การใช้ระบบ</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card m-2 shadow-none ">
                                            <div class="card-body ">
                                                <div class="flex-lg-row-fluid">
                                                    <div class="mb-13">
                                                        <div class="mb-15">
                                                            <h4 class="fs-2x text-gray-800 w-bolder mb-6">คำถามที่พบบ่อย</h4>
                                                        </div>
                                                        <div class="mb-15" id="faq-1">
                                                            <h3 class="text-gray-800 w-bolder mb-4">การเข้าสู่ระบบ</h3>
                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_1">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">How does it work?</h4>
                                                                </div>
                                                                <div id="kt_job_8_1" class="collapse show fs-6 ms-1">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                                <div class="separator separator-dashed"></div>
                                                            </div>
                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_2" aria-expanded="true">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>

                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>

                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">Do I need a designer to use Metronic?</h4>
                                                                </div>
                                                                <div id="kt_job_8_2" class="fs-6 ms-1 collapse show" style="">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                                <div class="separator separator-dashed"></div>
                                                            </div>

                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle collapsed mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_3">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">What do I need to do to start selling?</h4>
                                                                </div>
                                                                <div id="kt_job_8_3" class="collapse fs-6 ms-1">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                                <div class="separator separator-dashed"></div>
                                                            </div>

                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle mb-0 collapsed" data-bs-toggle="collapse" data-bs-target="#kt_job_8_4" aria-expanded="false">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">How much does Extended license cost?</h4>
                                                                </div>
                                                                <div id="kt_job_8_4" class="fs-6 ms-1 collapse" style="">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mb-15" id="faq-2">
                                                            <h3 class="text-gray-800 w-bolder mb-4">การใช้ระบบ</h3>
                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_1">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">How does it work?</h4>
                                                                </div>
                                                                <div id="kt_job_8_1" class="collapse show fs-6 ms-1">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                                <div class="separator separator-dashed"></div>
                                                            </div>
                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_2" aria-expanded="true">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>

                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>

                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">Do I need a designer to use Metronic?</h4>
                                                                </div>
                                                                <div id="kt_job_8_2" class="fs-6 ms-1 collapse show" style="">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                                <div class="separator separator-dashed"></div>
                                                            </div>

                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle collapsed mb-0" data-bs-toggle="collapse" data-bs-target="#kt_job_8_3">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">What do I need to do to start selling?</h4>
                                                                </div>
                                                                <div id="kt_job_8_3" class="collapse fs-6 ms-1">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                                <div class="separator separator-dashed"></div>
                                                            </div>

                                                            <div class="m-0">
                                                                <div class="d-flex align-items-center collapsible py-3 toggle mb-0 collapsed" data-bs-toggle="collapse" data-bs-target="#kt_job_8_4" aria-expanded="false">
                                                                    <div class="btn btn-sm btn-icon mw-20px btn-active-color-primary me-5">
                                                                        <span class="svg-icon toggle-on svg-icon-primary svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="6.0104" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="svg-icon toggle-off svg-icon-1">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"></rect>
                                                                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"></rect>
                                                                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"></rect>
                                                                            </svg>
                                                                        </span>
                                                                    </div>
                                                                    <h4 class="text-gray-700 fw-bolder cursor-pointer mb-0">How much does Extended license cost?</h4>
                                                                </div>
                                                                <div id="kt_job_8_4" class="fs-6 ms-1 collapse" style="">
                                                                    <div class="mb-4 text-gray-600 fw-bold fs-6 ps-10">First, a disclaimer – the entire process of writing a blog post often takes more than a couple of hours, even if you can type eighty words as per minute and your writing skills are sharp.</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="kt_modal_new_faq" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered mw-750px">
                                    <div class="modal-content rounded">
                                        <div class="modal-header pb-0 border-0 justify-content-end">
                                            <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                                <span class="svg-icon svg-icon-1">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
                                            <form id="kt_modal_new_ticket_form" class="form" action="#">
                                                <div class="mb-13 text-center">
                                                    <h1 class="mb-3">Create Ticket</h1>
                                                    <div class="text-gray-400 fw-bold fs-5">If you need more info, please check
                                                        <a href="" class="fw-bolder link-primary">Support Guidelines</a>.
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column mb-8 fv-row">
                                                    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                                        <span class="required">Subject</span>
                                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a subject for your issue"></i>
                                                    </label>
                                                    <input type="text" class="form-control form-control-solid" placeholder="Enter your ticket subject" name="subject" />
                                                </div>
                                                <div class="row g-9 mb-8">
                                                    <div class="col-md-6 fv-row">
                                                        <label class="required fs-6 fw-bold mb-2">Product</label>
                                                        <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Select a product" name="product">
                                                            <option value="">Select a product...</option>
                                                            <option value="1">HTML Theme</option>
                                                            <option value="1">Angular App</option>
                                                            <option value="1">Vue App</option>
                                                            <option value="1">React Theme</option>
                                                            <option value="1">Figma UI Kit</option>
                                                            <option value="3">Laravel App</option>
                                                            <option value="4">Blazor App</option>
                                                            <option value="5">Django App</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 fv-row">
                                                        <label class="required fs-6 fw-bold mb-2">Assign</label>
                                                        <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Select a Team Member" name="user">
                                                            <option value="">Select a user...</option>
                                                            <option value="1">Karina Clark</option>
                                                            <option value="2">Robert Doe</option>
                                                            <option value="3">Niel Owen</option>
                                                            <option value="4">Olivia Wild</option>
                                                            <option value="5">Sean Bean</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row g-9 mb-8">
                                                    <div class="col-md-6 fv-row">
                                                        <label class="required fs-6 fw-bold mb-2">Status</label>
                                                        <select class="form-select form-select-solid" data-control="select2" data-placeholder="Open" data-hide-search="true">
                                                            <option value=""></option>
                                                            <option value="1" selected="selected">Open</option>
                                                            <option value="2">Pending</option>
                                                            <option value="3">Resolved</option>
                                                            <option value="3">Closed</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 fv-row">
                                                        <label class="required fs-6 fw-bold mb-2">Due Date</label>
                                                        <div class="position-relative d-flex align-items-center">
                                                            <div class="symbol symbol-20px me-4 position-absolute ms-4">
                                                                <span class="symbol-label bg-secondary">
                                                                    <span class="svg-icon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                            <rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
                                                                            <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
                                                                            <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
                                                                            <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
                                                                        </svg>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <input class="form-control form-control-solid ps-12" placeholder="Select a date" name="due_date" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-column mb-8 fv-row">
                                                    <label class="fs-6 fw-bold mb-2">Description</label>
                                                    <textarea class="form-control form-control-solid" rows="4" name="description" placeholder="Type your ticket description"></textarea>
                                                </div>
                                                <div class="fv-row mb-8">
                                                    <label class="fs-6 fw-bold mb-2">Attachments</label>
                                                    <div class="dropzone" id="kt_modal_create_ticket_attachments">
                                                        <div class="dz-message needsclick align-items-center">
                                                            <span class="svg-icon svg-icon-3hx svg-icon-primary">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 12.6L12.7 9.3C12.3 8.9 11.7 8.9 11.3 9.3L8 12.6H11V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18V12.6H16Z" fill="black" />
                                                                    <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            <div class="ms-4">
                                                                <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                                                                <span class="fw-bold fs-7 text-gray-400">Upload up to 10 files</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="text-center">
                                                    <button type="reset" id="kt_modal_new_ticket_cancel" class="btn btn-light me-3">Cancel</button>
                                                    <button type="submit" id="kt_modal_new_ticket_submit" class="btn btn-primary">
                                                        <span class="indicator-label">Submit</span>
                                                        <span class="indicator-progress">Please wait...
                                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <?php include 'layouts/inc-footer.php' ?>
            </div>
            <!--end::Wrapper-->
        </div>
    </div>
    <?php include 'layouts/inc-modal-plugin.php' ?>
    <?php include 'layouts/inc-script.php' ?>
</body>

</html>