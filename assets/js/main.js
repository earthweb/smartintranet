var ctx = document.getElementById("kt_chartjs_vote");

// Define colors
var dangerColor = KTUtil.getCssVariableValue("--bs-danger");
var successColor = KTUtil.getCssVariableValue("--bs-success");
var warningColor = KTUtil.getCssVariableValue("--bs-warning");

// Define fonts
var fontFamily = KTUtil.getCssVariableValue("--bs-font-sans-serif");

// Chart labels
const labels = ["พอใจมาก", "พอใจปานกลาง", "พอใจน้อย"];

// Chart data
const data = {
  labels: labels,
  datasets: [
    {
      data: [10, 20, 30],
    },
  ],
};

// Chart config
const config = {
  type: "doughnut",
  data: {
    labels: labels,
    datasets: [{
        backgroundColor: [successColor,warningColor,dangerColor],
        data: [50, 60, 70],
      },
    ],
  },
};
// Init ChartJS -- for more info, please visit: https://www.chartjs.org/docs/latest/
var myChart = new Chart(ctx, config);